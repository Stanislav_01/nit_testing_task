const testGame = require("./index.js");

test("created correct", () => {
  let testGameInstanse = new testGame(4);
  expect(testGameInstanse.scoreArray).toEqual([301, 301, 301, 301]);
});

test("exception in case a winner has been identified but someone named the method", () => {
  let testGameInstanse = new testGame(4);
  testGameInstanse.scoreArray = [0, 301, 301, 301];
  expect(() => testGameInstanse.throw(0, 0, 2)).toThrow();
});

test(`if we don't have a player with such an index, he should throw an exception`, () => {
  let testGameInstanse = new testGame(4);
  expect(() => testGameInstanse.score(5)).toThrow(
    `Player  ${5}, you don't exist :-( `
  );
});

test("throw function works normall", () => {
  let testGameInstanse = new testGame(4);
  testGameInstanse.scoreArray = [301, 301, 301, 301];
  testGameInstanse.throw(20, 2, 0);
  expect(testGameInstanse.scoreArray).toEqual([261, 301, 301, 301]);
});

test("score function works normall", () => {
  let testGameInstanse = new testGame(4);
  testGameInstanse.scoreArray = [301, 301, 301, 301];
  expect(testGameInstanse.score(1)).toBe(301);
});
