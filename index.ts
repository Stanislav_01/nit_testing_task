class Game {
  number: number;
  scoreArray: number[];

  constructor(number: number) {
    this.scoreArray = new Array(number).fill(301);
  }

  throw(sectorNumber: number, multi: number, playerIndex: number) {
    let gameResult: string;

    this.scoreArray[playerIndex] =
      this.scoreArray[playerIndex] - sectorNumber * multi;

    this.scoreArray.forEach((result: number) => {
      if (!result) {
        throw new Error(`Game over`);
      }
    });

    if (this.scoreArray[playerIndex] < 0) {
      this.scoreArray[playerIndex] = 301;
      gameResult = `Player ${playerIndex}, you start the game again`;
    }
    if (gameResult) {
      return gameResult;
    }
  }

  score(playerIndex: number) {
    let result;
    if (this.scoreArray[playerIndex]) {
      result = this.scoreArray[playerIndex];
      return result;
    } else {
      throw new Error(`Player  ${playerIndex}, you don't exist :-( `);
    }
  }
}

module.exports = Game;
